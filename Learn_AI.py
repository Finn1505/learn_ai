import string, math, random, neat, os, sys, pickle, graphviz, warnings

class AI:

    def __init__(self, config, genome):
        self.genome = genome
        self.config = config
        self.net = neat.nn.feed_forward.FeedForwardNetwork.create(genome, config) if genome != None and config != None else None          # neat.nn.feed_forward.FeedForwardNetwork.create  neat.nn.recurrent.RecurrentNetwork.create
        self.choice = None

        genome.fitness = 0 if genome.fitness == None else genome.fitness

    def make_choice(self, x, y, operation):
        
        ai_output = self.net.activate((x, y, operation))

        self.choice = ai_output[0]

# Train AI
def train_ai(ais):   
        
    # Iterate over ais
    for ai_nr in range(len(ais)):

        # Operation
        #operation_nr = random.randint(1,2)  # 1 == Add; 2 == Sub
        points = 0
        for i in range(1,4,1):    #1,5
            operation_nr = i
            
            # Get random numbers
            rdm_x = random.randint(100, 200)
            rdm_y = random.randint(20, 50)
            result = None
            accuracy = None

            # Get Result
            # Add
            if operation_nr == 1:
                result = rdm_x + rdm_y
                accuracy = 1

            # Sub
            elif operation_nr == 2:
                result = rdm_x - rdm_y
                accuracy = 1

            # Mul
            elif operation_nr == 3:
                result = rdm_x * rdm_y
                accuracy = 1

            # Div
            elif operation_nr == 4:
                result = rdm_x / rdm_y
                accuracy = 0.01

            # Let the ai make a choice
            ais[ai_nr].make_choice(x=rdm_x, y=rdm_y, operation=operation_nr)

            # Check if ai makes the correct choice
            # Correct
            #ais[ai_nr].genome.fitness += -abs(ais[ai_nr].choice - result) + accuracy #random.uniform(1, 1)
            if -abs(ais[ai_nr].choice - result) + accuracy > 0: # Correct
                points += -abs(ais[ai_nr].choice - result) + accuracy
            else:

                if operation_nr == 3:
                    points += (-abs(ais[ai_nr].choice - result) / rdm_y ) * 3
                else:
                    points += -abs(ais[ai_nr].choice - result) * 3
                

        ais[ai_nr].genome.fitness += points

def eval_genomes(genomes, config):


    ais = []
    # Iterate over genomes
    for i, (genome_id1, genome1) in enumerate(genomes):

       new_ai = AI(config, genome1)
       ais.append(new_ai)

    # Train ais
    train_ai(ais)

    return  


'''
Activation Functions (Custom)
'''

def my_add(x):
    return 1 if x == 1 else 0

def my_sub(x):
    return 1 if x == 2 else 0

'''
Aggregation Functions (Custom)
'''
def my_agg_just_add(x):
    if x[0] >= 1.0 and x[0] <= 1.0:

        #if len(x) == 2:
        #    return x[1] 

        if len(x) == 3:
            return (x[1] + x[2]) 

    return 0

def my_agg_just_sub(x):
    if x[0] >= 2.0 and x[0] <= 2.0:

        #if len(x) == 2:
        #    return x[1] 

        if len(x) == 3:
            return (x[1] - x[2]) 

    return 0

def my_agg_just_mul(x):
    if x[0] >= 3.0 and x[0] <= 3.0:

        #if len(x) == 2:
        #    return x[1] 

        if len(x) == 3:
            return (x[1] * x[2]) 

    return 0

def my_agg_just_div(x):
    if x[0] >= 4.0 and x[0] <= 4.0:

        if len(x) == 3:
            
            # No division by 0
            if x[2] == 0:
                return 0

            return (x[1] / x[2]) 

    return 0


def my_agg_just_add_sub(x):

    # Add
    if x[0] >= 1.0 and x[0] <= 1.0:

        #if len(x) == 2:
        #    return x[1] 

        if len(x) == 3:
            return (x[1] + x[2]) 
    
    # Sub
    elif x[0] >= 2.0 and x[0] <= 2.0:

        #if len(x) == 2:
        #    return x[1] 

        if len(x) == 3:
            return (x[1] - x[2]) 

    return 0


def run_neat(config):

    name = 'AI_Learn__Simple_Add_Minus_1_'

    #p = neat.Checkpointer.restore_checkpoint(name)
    p = neat.Population(config)
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(generation_interval=100000, time_interval_seconds=600, filename_prefix=name))

    winner = p.run(eval_genomes, 1_000_000)
    with open("best." + name, "wb") as f:
        pickle.dump(winner, f)


    # Draw graph
    # Create a Digraph object

    """Receives a neural network and draws a Graphviz representation of the connections.
    If view is set to true, the generated image is displayed.
    """
    nodes = set()
    edges = []
    for conn in winner.connections.values():
        if conn.enabled:
            edges.append((conn.key[0], conn.key[1], conn.weight))
            nodes.add(conn.key[0])
            nodes.add(conn.key[1])

    dot = graphviz.Digraph(comment='Neural Network')
    for node in nodes:
        dot.node(str(node))

    for e in edges:
        dot.edge(str(e[0]), str(e[1]), label="{:.2f}".format(e[2]), penwidth=str(abs(e[2])))

    dot.render('ai_graph', view=False)

    return
    
def test_ai(config):

    print("Test_AI")
    with open("best.AI_Learn__Simple_Add_Minus_1_", "rb") as f:
        winner = pickle.load(f)
    #winner_net = neat.nn.recurrent.RecurrentNetwork.create(winner, config)

    ai = AI(config, winner)

    while(True):
        # Operation
        operation_nr = random.randint(1,4)  # 1 == Add; 2 == Sub

        # Get random numbers
        rdm_x = random.randint(0, 1000)
        rdm_y = random.randint(100, 1000)
        result = None

        # Get Result
        # Add
        if operation_nr == 1:
             result = rdm_x + rdm_y

        # Sub
        elif operation_nr == 2:
            result = rdm_x - rdm_y

        # Mult
        elif operation_nr == 3:
            result = rdm_x * rdm_y

        # Div
        elif operation_nr == 4:
            result = rdm_x / rdm_y

        # Let the ai make a choice
        ai.make_choice(x=rdm_x, y=rdm_y, operation=operation_nr)

        ai_choice = ai.choice
        
    

    return


if __name__ == "__main__":

    
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, "config.txt")

    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_path)

    'Add custom functions'
    config.genome_config.add_activation('my_add_sub', my_add)
    config.genome_config.add_aggregation('my_agg_just_add', my_agg_just_add)
    config.genome_config.add_aggregation('my_agg_just_sub', my_agg_just_sub)
    config.genome_config.add_aggregation('my_agg_just_mul', my_agg_just_mul)
    config.genome_config.add_aggregation('my_agg_just_div', my_agg_just_div)
    config.genome_config.add_aggregation('my_agg_just_add_sub', my_agg_just_add_sub)


    run_neat(config)
    #test_ai(config)





